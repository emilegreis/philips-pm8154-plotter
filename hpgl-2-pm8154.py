#!/usr/bin/env python

import sys
from pm8154 import PM8154

plotter = PM8154()

# raw hpgl to pm8154 converter

# sleep time after actions
tpu = 0.6
tpd = 0.15
tmv = 0.15

plotter.pen(1)
hpgl = str(sys.stdin.read())
commands = hpgl.split(';')

print('Commands:', len(commands))

# iterate and write commands to plotter
i=0
for command in commands:
    #if i < 835: # restart start at specific command number
    #    i+=1
    #    continue
    instruction = command[:2]
    if instruction == 'PU':
        plotter._write('H', tpu) # long sleep after each penup to prevent buffer overflow
        print(i, 'PU / H')
    elif instruction =='PD':
        plotter._write('I', tpd) # short sleep after pendown to prevent ink diffusion on paper (marker pen)
        print(i, 'PD / I')
    if instruction in ['PU', 'PD']:
        if len(command) > 2:
            coordinates = command[2:].split(',')
            for j in range(0, len(coordinates), 2):
                x,y = coordinates[j],coordinates[j+1]
                print(i, x, y)
                plotter._write(f"{x}/{y}K", tmv) # same
    i+=1

# TODO: listen for space bar input to pause / start 
