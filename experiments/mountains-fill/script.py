#!/usr/bin/env python

import sys
sys.path.insert(1, '../../')

from pm8154 import PM8154
import random

plotter = PM8154()


# a region fill experiment

def mountains_fill_x(x,y,w,h,t,sections=4,amplitude=10,precision=3):
    plotter.PU()

    q=round(h/t)

    # start points
    nextPoints = []
    for i in range(q):
        nextPoints.append(int(x-t/2))

    # middle points 
    sectionSize = round(w/sections) 
    for j in range(sections-1):
        prevPoints = nextPoints
        nextPoints = []
        nextPoints.append(x+sectionSize*(j+1)+random.randrange(-amplitude,+amplitude+1))
        for i in range(1,q):
            point = random.randrange(nextPoints[i-1]-precision,nextPoints[i-1]+precision+1)
            while not x+sectionSize*(j+1)-amplitude <= point <= x+sectionSize*(j+1)+amplitude+1:
                point = random.randrange(nextPoints[i-1]-precision,nextPoints[i-1]+precision+1)
            nextPoints.append(point)
        for i in range(len(nextPoints)):
            print(prevPoints[i],nextPoints[i])
            plotter.move(int(prevPoints[i]+t/2),y+i*t)
            plotter.PD()
            plotter.move(int(nextPoints[i]-t/2),y+i*t)
            plotter.PU()
    
    # end points
    prevPoints = nextPoints
    nextPoints = []
    for i in range(q):
        nextPoints.append(x+w)
        print(prevPoints[i],nextPoints[i])
        plotter.move(int(prevPoints[i]+t/2),y+i*t)
        plotter.PD()
        plotter.move(nextPoints[i],y+i*t)
        plotter.PU()


plotter.pen(1)
mountains_fill_x(0,0,2300,1800,8,6,80,16)
