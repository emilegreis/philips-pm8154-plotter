#!/usr/bin/env python

import sys
sys.path.insert(1, '../../')

import os
from pm8154 import PM8154
from PIL import Image
import drawsvg as draw

plotter = PM8154()



# First prep a grayscale (100px wide) dithered image 
# with imagick
#
#     magick input.png -resize 100x100 outfile.bmp
#     magick input.png -dither FloydSteinberg -colors 8 outfile.bmp
#
# or gimp
#
#    colors > dither


# Define the period (space between each dot): 100*23=2300
period = 23


# Define contact times depending on the number of different values of greys (ignore 255), paper, pen and ink
times = [
    0,
    0.05,
    0.5,
    1,
    1.5,
    2,
    2.5
]
times.sort()
print(times)




try:
    sys.argv[1]
except:
    print('missing argument 1: image input file')
else:

    d = draw.Drawing(2300, 1800, origin=(0,0)) # for testing purpose before printing

    with Image.open(sys.argv[1]) as img:
        print(img.size)
        pixels = img.load()
       
        # get all different values
        values = []
        for x in range(img.size[0]):
            for y in range(img.size[1]):
                v = pixels[x,y]
                if v not in values:
                    values.append(v)
        values.sort(reverse=True)
        values.pop(0)
        print(values)
       
        # print each pixels
        for x in range(img.size[0]):
            for y in range(img.size[1]):
                v = pixels[x,y]
                if v != 255:
                    t = times[values.index(v)]
                    ry = img.size[1]-1-y # reverse y axis for plotter
                    print(x, y, ry, ':', v, t)
                    d.append(draw.Circle(x*period, y*period, t*2+2, fill='black', stroke_width=0)) # testing
                    plotter.point(x*period, ry*period, t)

        # testing
        d.save_svg('example.svg')
        d.save_png('example.png')
