#!/usr/bin/env python

import sys
sys.path.insert(1, '../../')

import os
from pm8154 import PM8154
from PIL import Image
import drawsvg as draw

plotter = PM8154()



def rgb2hex(r, g, b):
    return '#{:02x}{:02x}{:02x}'.format(r, g, b)


def get_values(path):
    with Image.open(path) as img:
        print(img.size)
        pixels = img.load()
        values = []
        for x in range(img.size[0]):
            for y in range(img.size[1]):
                r, g, b = pixels[x,y]
                v = rgb2hex(r,g,b)
                if v not in values:
                    values.append(v)
        d = draw.Drawing(100, len(values)*10, origin=(0,0)) # colors palette 
        values.sort()
        print(values)


period = 10 # Define the period (space between each dot): 100*23=2300
colors = { # Define colors and pens
          '#2b2728':1, # noir
          '#55657d':2, # bleu
          '#8b4a24':3, # marron
          '#97a09f':4, # gris
          '#d1d0c7':5  # blanc
}
yoffset = 0
#colors = {'#525a6c', '#636e81', '#665043', '#8d918e', '#a7acaa'}
print(colors)


def plot(path):
    d = draw.Drawing(2300, 1800, origin=(0,0)) # for testing purpose before printing
    with Image.open(path) as img:
        print(img.size)
        pixels = img.load()
        for c in colors:
        #for c in ['#121715']:
            #plotter.pen(colors[c]) # Take corresponding pen
            for x in range(img.size[0]):
                for y in range(img.size[1]):
                    r, g, b = pixels[x,y]
                    v = rgb2hex(r,g,b)
                    if v == c:
                        print(x, y, ':', v)
                        d.append(draw.Circle(x*period, y*period, period/2, fill=c, stroke_width=0)) # testing
                        #d.append(draw.Rectangle(x*period, y*period, period, period, fill=c, stroke_width=0)) # testing
                        ry = img.size[1]-1-y # reverse y axis for plotter
                        #plotter.point(x*period, ry*period, 0)
    # test image
    d.save_svg('test.svg')
    d.save_png('test.png')



def plot_ylines(path):
    d = draw.Drawing(2300, 1800, origin=(0,0)) # for testing purpose before printing
    with Image.open(path) as img:
        print(img.size)
        pixels = img.load()
        for c in colors:
        #for c in ['#121715']:
            #plotter.pen(colors[c]) # Take corresponding pen
            for x in range(img.size[0]):
                points = []
                for y in range(img.size[1]):
                    r, g, b = pixels[x,y]
                    v = rgb2hex(r,g,b)
                    if v == c:
                        print(x, y, ':', v)
                        points.append([x,y])
                    if v != c or (v == c and y == img.size[1]-1):
                        if len(points) == 1:
                            d.append(draw.Circle(points[0][0]*period, points[0][1]*period, period/2, fill=c, stroke_width=0)) # testing
                            #d.append(draw.Rectangle(points[0][0]*period, points[0][1]*period, period, period, fill=c, stroke_width=0)) # testing
                            
                            py = img.size[1]-1-points[0][1] # reverse y axis for plotter
                            #plotter.point(points[0][0]*period, py*period, 0)
                        elif len(points) > 1:
                            #d.append(draw.Line(points[0][0]*period+period/2,points[0][1]*period,points[-1][0]*period+period/2,points[-1][1]*period+period,stroke=c, stroke_width=period))
                            d.append(draw.Line(points[0][0]*period, points[0][1]*period, points[-1][0]*period, points[-1][1]*period, stroke=c, stroke_width=period, stroke_linecap='round' ))
                            #for i in range(len(points)):
                            #    d.append(draw.Rectangle(points[i][0]*period, points[i][1]*period, period, period, fill=c, stroke_width=0)) # testing

                            py1 = img.size[1]-1-points[0][1] #reverse y axis for plotter, first point
                            py2 = img.size[1]-1-points[-1][1] #reverse y axis for plotter, last point
                            #plotter.line(points[0][0]*period, py1*period, points[-1][0]*period,)
                        points = []
    # test image
    #d.save_svg('test.svg')
    d.save_png('test.png')



def plot_xlines(path):
    d = draw.Drawing(2300, 1800, origin=(0,0)) # for testing purpose before printing
    with Image.open(path) as img:
        print(img.size)
        pixels = img.load()
        #for c in colors:
        for c in ['#2b2728']:
            #plotter.pen(colors[c]) # Take corresponding pen
            for y in range(img.size[1]):
                points = []
                for x in range(img.size[0]):
                    r, g, b = pixels[x,y]
                    v = rgb2hex(r,g,b)
                    if v == c:
                        print(x, y, ':', v)
                        points.append([x,y])
                    if v != c or (v == c and x == img.size[0]-1):
                        if len(points) == 1:
                            d.append(draw.Circle(points[0][0]*period, points[0][1]*period+yoffset, period/2, fill=c, stroke_width=0)) # testing
                            #d.append(draw.Rectangle(points[0][0]*period, points[0][1]*period, period, period, fill=c, stroke_width=0)) # testing
                            
                            py = img.size[1]-1-points[0][1] # reverse y axis for plotter
                            plotter.point(points[0][0]*period, py*period+yoffset, 0)
                        elif len(points) > 1:
                            #d.append(draw.Line(points[0][0]*period+period/2,points[0][1]*period,points[-1][0]*period+period/2,points[-1][1]*period+period,stroke=c, stroke_width=period))
                            d.append(draw.Line(points[0][0]*period, points[0][1]*period+yoffset, points[-1][0]*period, points[-1][1]*period+yoffset, stroke=c, stroke_width=period, stroke_linecap='round' ))
                            #for i in range(len(points)):
                            #    d.append(draw.Rectangle(points[i][0]*period, points[i][1]*period, period, period, fill=c, stroke_width=0)) # testing

                            py1 = img.size[1]-1-points[0][1] #reverse y axis for plotter, first point
                            py2 = img.size[1]-1-points[-1][1] #reverse y axis for plotter, last point
                            plotter.line(points[0][0]*period, py1*period+yoffset, points[-1][0]*period+yoffset, py2*period)

                        points = []
        
    # test image
    #d.save_svg('test.svg')
    d.save_png('test.png')




try:
    sys.argv[1]
except:
    print('missing argument 1: image input file')
else:
    # First get all different values of an image then define the colors array line 40
    get_values(sys.argv[1])
    # Then print
    #plot(sys.argv[1])
    #plot_ylines(sys.argv[1])
    #plot_xlines(sys.argv[1])
