#convert inputs/mont-odile.jpg -resize 200x200 -quality 100 -rotate -90 resized.jpg
#convert inputs/ship1.jpg -resize 230x180 -quality 100 resized.jpg

## Colors channels
#convert test_resized.jpg -colorspace cmyk -channel c -negate -separate test_c.jpg
#convert test_resized.jpg -colorspace cmyk -channel m -negate -separate test_m.jpg
#convert test_resized.jpg -colorspace cmyk -channel y -negate -separate test_y.jpg
#convert test_resized.jpg -colorspace cmyk -channel k -negate -separate test_k.jpg

## Monochrome dithered bitmap images
#convert test_c.jpg -monochrome test_c_monochrome.jpg
#convert test.jpg -remap pattern:gray50 test_monochromejpg

# Diffused Pixel Dithering
#convert resized.jpg -ordered-dither threshold test.jpg

# Ordered Dither using Uniform Color Levels
#convert resized.jpg -ordered-dither threshold,4 test.jpg

# Tests
#for s in rgb ycc;do
#    convert resized.jpg -quantize $s -depth 8 +dither -colors 4 imagemagick-4c-$s-nodith.png
#    convert resized.jpg -quantize $s -depth 8 -dither FloydSteinberg -colors 4 imagemagick-4c-$s-floydsteinberg.png
#    convert resized.jpg -quantize $s -depth 8 -dither Riemersma -colors 4 imagemagick-4c-$s-riemersma.png
#    for d in o2x2 o3x3 o4x4 o8x8 h4x4a h6x6a h8x8a h4x4o h6x6o h8x8o h16x16o checks;do
#        convert resized.jpg -quantize $s -depth 8 -ordered-dither $d -colors 4 imagemagick-4c-$s-$d.png
#    done
#done


# RSCOLORQ
#./rscolorq/rscolorq -i input-resized.jpg -o outputs/rscolorq-rgb-4-colors.png -n 4 --auto -s 0 --colors FF00FF,00FFFF,FFFF00,000000
./rscolorq/rscolorq -i resized.jpg -o outputs/plotter1.png -n 6 --auto -s 0 --iters 5 --repeats 3 --op palette.png
