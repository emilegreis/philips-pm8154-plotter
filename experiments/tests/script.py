#!/usr/bin/env python

import sys
sys.path.insert(1, '../../')

from pm8154 import PM8154

plotter = PM8154()



# Test the diffusion of ink on paper by incrementing the pen contact time

def diffusion_line(x, y, quantity, increment, spacing):
    for i in range(quantity):
        print(i)
        plotter.point(x+i*spacing, y, i*increment)
    plotter.text(x+i*spacing+40, y, str(quantity) + " / " + str(increment) + " / " + str(spacing), 40, 40)




# Test the variable width and height of characters

def character_grid(x, y, text='R', columns=4, rows=4, width_start=50, height_start=50, width_increment=10, height_increment=10, spacing=40, slant=0):
    widths_increments = 0
    for i in range(columns):
        widths_increments = widths_increments + width_increment*i
        heights_increments = 0
        for j in range(rows):
            heights_increments = heights_increments + height_increment*j
            plotter.text(x+width_start*len(text)*i+spacing*i+widths_increments*len(text), y-height_start*j-spacing*j-heights_increments, text, width_start+width_increment*i, height_start+height_increment*j, slant)
            # TODO multi characters


plotter.pen(1)
#diffusion_line(900, 20, 10, 0.0, 8)

x = 1200
y = 900
for i in range(10):
    plotter.point(x+i*10, y, 0)

#plotter.rect(0,0,2300,1800)
