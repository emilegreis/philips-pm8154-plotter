#!/usr/bin/env python

import sys
sys.path.insert(1, '../../')

from pm8154 import PM8154
import math
import random

plotter = PM8154()


# some basics regions / rectangle fills

def x_lines(x,y,w,h,t):
    q=round(h/t)
    for i in range(q+1):
        py=y+i*t
        plotter.PU()
        plotter.move(x, py)
        plotter.PD()
        plotter.move(x+w, py)
    plotter.PU()

def y_lines(x,y,w,h,t):
    q=round(w/t)
    for i in range(q+1):
        px=x+i*t
        plotter.PU()
        plotter.move(px, y)
        plotter.PD()
        plotter.move(px, y+h)
    plotter.PU()

def inset(x,y,w,h,t):
    q=round(min(w,h)/2/t)
    for i in range(q+1):
        px=x+i*t
        py=y+i*t
        pw=w-i*t*2
        ph=h-i*t*2
        plotter.PD()
        plotter.rect(px,py,pw,ph)
    plotter.PU()

def spiral(x,y,w,h,t):
    plotter.PU()
    plotter.move(x,y)
    plotter.PD()
    q=round(min(w,h)/2/t)
    for i in range(q+1):
        plotter.move(x+i*t,y+h-i*t)
        plotter.move(x+w-i*t,y+h-i*t)
        plotter.move(x+w-i*t,y+i*t)
        plotter.move(x+i*t+t,y+i*t)
    plotter.PU()


plotter.pen(1)
#x_lines(300,0,300,200,8)
#y_lines(900,0,200,200,8)
#inset(1700,0,240,200,8)
#spiral(600,250,190,180,8)
