#!/usr/bin/env python

from pm8154 import PM8154
import math
import turtle

plotter = PM8154()

# Turtle tests
screen = turtle.Screen()
screen.setup(width=2300,height=1800,startx=0, starty=0)
#screen.setworldcoordinates(0, 0, 2300, 1800)
screen.bgcolor("white")
pen = turtle.Turtle()
pen.speed(10)
pen.color("blue")
pen.pensize(1)
pen.hideturtle()
pen.penup()





# https://math.libretexts.org/Bookshelves/Calculus/Calculus_3e_(Apex)/09%3A_Curves_in_the_Plane/9.02%3A_Parametric_Equations

def parabola(rt, a=1, xo=0, yo=0):

    x=-rt*-rt*a+xo
    y=0+yo
    pen.goto(x,y)
    #plotter._write(f"H {x}/{y} K")

    for t in range(-rt, rt+1):
        x=t*t*a
        y=t
        x=x+xo
        y=y+rt+yo
        print(x,y)
        pen.pendown()
        pen.goto(x,y)
        #plotter._write(f"I {x}/{y} K")
    #plotter._write("H")



def lissajous(rt, a, b, xo=0, yo=0):

    A=100
    B=100
    t=0
    
    for i in range(0, rt):
        t+=0.01
        d=math.pi/2
        x=A*math.sin(a*t+d)
        y=B*math.sin(b*t)
        print(i, x+xo,y+yo)
        pen.goto(x,y)
        pen.pendown()
        pen.getscreen().update()



def rosecurve(rt, xo=0, yo=0):

    for t in range(-rt, rt):
        x=math.cos(t)*math.sin(4*t)
        y=math.sin(t)*math.cos(4*t)
        print(x+xo,y+yo)
        pen.pendown()
        pen.goto(x*50,y*50)


def hypotrochoid(rt, xo=0, yo=0):

    for t in range(-rt, rt):
        x=2*math.cos(t)+5*math.cos(2*t/3)
        y=2*math.sin(t)-5*math.sin(2*t/3)
        print(x+xo,y+yo)
        pen.pendown()
        pen.goto(x*30,y*30)





def epicycloid(rt, xo=0, yo=0):

    for t in range(-rt, rt):
        x=4*math.cos(t)-math.cos(4*t)
        y=4*math.sin(t)-math.sin(4*t)
        print(x,y)
        pen.pendown()
        pen.goto(x*30,y*30)


#parabola(900, 0.002)
lissajous(630, 5, 4)


#hypotrochoid(200)
#rosecurve(100)

#epicycloid(100)


turtle.mainloop()

# maurer rose
# bowditch curve
# butterfly curve
# erdos lemniscate
