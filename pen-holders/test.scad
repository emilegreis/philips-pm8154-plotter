
module tube(height, radius, thickness) {
    difference() {
        cylinder(height, radius, radius, false);
        translate([0,0,-1])
        cylinder(height+2, radius-thickness, radius-thickness, false);
    }
}



tube(24, 7.5, 1);
translate([0,0,7])
tube(2, 8.5, 1);
translate([0,0,11])
tube(7, 8.5,1);
translate([0,0,20])
tube(2, 8.5, 1);
