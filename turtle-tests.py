#!/usr/bin/env python

#from pm8154 import PM8154
import math
import subprocess
import turtle

#plotter = PM8154()

# Turtle 
turtle.resizemode("user")
screen = turtle.Screen()
screen.setup(width=2300,height=1800,startx=0, starty=0)
screen.setworldcoordinates(0, 0, 2300, 1800)
screen.bgcolor("white")
pen = turtle.Turtle()
pen.speed(50)
pen.color("blue")
pen.pensize(1)
pen.hideturtle()
pen.penup()



def nothing():
    return

matchs = {
        'PU': pen.penup,
        'PD': pen.pendown,
        'PA': nothing
        }

# Plotter canvas
pen.pensize(4)
pen.color("red")
pen.goto(0,0)
pen.pendown()
pen.goto(0,1800)
pen.goto(2300,1800)
pen.goto(2300,0)
pen.goto(0,0)
pen.penup()

# Hershey test
pen.color("blue")
text='Hershey'
size = 0.04

xpos = 0
ypos = 0
xzero = 0
yzero = 0

cmd1 = subprocess.run(['printf', '.PS\n "'+text+'" \n.PE'], capture_output=True)
cmd2 = subprocess.run(['pic2plot', '-F', 'HersheySans', '-f', str(size), '-T', 'hpgl', '--rotation', '0'], input=cmd1.stdout, capture_output=True)
hpgl = cmd2.stdout.decode('utf8')

commands = hpgl.split(';')

xoffset = math.inf
yoffset = math.inf

# find small x coordinate
for command in commands:
    instruction = command[:2]
    if instruction in matchs:
        if len(command) > 2:
            coordinates = command[2:].split(',')
            for i in range(0, len(coordinates), 2):
                x,y = int(coordinates[i]),int(coordinates[i+1])
                if x and x < xoffset:
                    xoffset = x
                if y and y < yoffset:
                    yoffset = y

# plot coordinates
for command in commands:
    instruction = command[:2]
    if instruction in matchs:
        print()
        matchs[instruction]()
        print(instruction)
        if len(command) > 2:
            coordinates = command[2:].split(',')
            for i in range(0, len(coordinates), 2):
                x,y = int(coordinates[i]),int(coordinates[i+1])
                print(x,y)
                #pen.goto((x-xoffset)+xpos,(y-yoffset+(27.38*size*100))*1.277777+ypos)
                #pen.goto((x-xoffset)+xpos,(y-yoffset)+ypos*1.27777)
                pen.goto((x-xoffset)+xpos+xzero,(y-yoffset)+ypos+yzero)

print('offsets', xoffset, yoffset)










print('done')
turtle.mainloop()

