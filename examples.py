#!/usr/bin/env python

import pm8154
from pm8154 import PM8154
import random
import time


plotter = PM8154()

# Start plotting

print(plotter.serial_port)


# A random move
x = random.randint(0, plotter.width)
y = random.randint(0, plotter.height)
print(x,y)
plotter.move(x,y)

# A random point
#x = random.randint(0, plotter.width)
#y = random.randint(0, plotter.height)
#plotter.point(x,y,0.2)

# A random line
#x1 = random.randint(0, plotter.width)
#y1 = random.randint(0, plotter.height)
#x2 = random.randint(0, plotter.width)
#y2 = random.randint(0, plotter.height)
#print(x1,y1,x2,y2)
#plotter.line(x1,y1,x2,y2)

# A random rect
#w = random.randint(100, 400)
#h = random.randint(100, 400)
#x = random.randint(0, plotter.width-w)
#y = random.randint(0, plotter.height-h)
#plotter.rect(x,y,w,h)

# A random circle
#r = random.randint(100, 400)
#x = random.randint(r*2, plotter.width)
#y = random.randint(r, plotter.height-r)
#print(x,y,r)
#plotter.circle(x,y,r)

# A random arc
#r = random.randint(100, 400)
#s = random.randint(0, 180)
#f = random.randint(s, 359)
#x = random.randint(r*2, plotter.width)
#y = random.randint(r, plotter.height-r)
#print(x,y,r,s,f)
#plotter.arc(x,y,r,s,f)

# 10 random rect
#for i in range(10):
#    w = random.randint(100, 400)
#    h = random.randint(100, 400)
#    x = random.randint(0, plotter.width-w)
#    y = random.randint(0, plotter.height-h)
#    plotter.rect(x,y,w,h)

# 10 random circles
#for i in range(10):
#    r = random.randint(100, 400)
#    x = random.randint(r*2, plotter.width)
#    y = random.randint(r, plotter.height-r)
#    plotter.circle(x,y,r)

# Text
#x = random.randint(0, plotter.width)
#y = random.randint(0, plotter.height)
#plotter.text(x,y,'PM8154', 60, 60, 0, 1)

# Hershey
#x = 100
#y = 100
#plotter.hershey(x,y,'PM8154', 'Serif-Bold', 64)


# Hershey rotate
#x = 1350
#y = 1200
#for i in range(0, 359, 36):
#     plotter.hershey(x,y,'......Rotate ' + str(i), 'Sans', 64, str(i))


