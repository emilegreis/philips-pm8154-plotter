#!/usr/bin/env python

import serial
import subprocess
import time 
import math



hershey = [
    'Serif',
    'Serif-Italic',
    'Serif-Bold',
    'Serif-BoldItalic',
    'Cyrillic',
    'Cyrillic-Oblique',
    'EUC',
    'Sans',
    'Sans-Oblique',
    'Sans-Bold',
    'Sans-BoldOblique',
    'Script',
    'Script-Bold',
    'GothicEnglish',
    'GothicGerman',
    'GothicItalian',
    'SerifSymbol',
    'SerifSymbol-Oblique',
    'SerifSymbol-Bold',
    'SerifSymbol-BoldOblique',
    'SansSymbol',
    'SansSymbol-Oblique'
]



class PM8154():
    def __init__(self) -> None:
        self.serial_port = serial.Serial(
            port="/dev/ttyUSB0",
            baudrate=9600,
            bytesize=serial.SEVENBITS,
            parity=serial.PARITY_EVEN,
            stopbits=serial.STOPBITS_ONE,
            xonxoff=True,
            timeout=1
        )
        self.width = 2300
        self.height = 1800

    def _write(self, command, pause=0.2):
        command_in_bytes = str.encode(command)
        self.serial_port.write(command_in_bytes)
        time.sleep(pause)
    
    def pen(self, n=0):
        print(f"F{n}")
        self._write(f"F{n}")
        self._write(f"F{n}") # idk??

    def PU(self):
        self._write("H")

    def PD(self):
        self._write("I")
    
    def move(self, x, y):
        self._write(f"{x}/{y}K")

    def point(self, x, y, contact=1):
        self._write(f"{x}/{y}K")
        self._write("I")
        time.sleep(contact)
        self._write("H")

    def line(self, x1, y1, x2, y2):
        self._write(f"{x1}/{y1}K")
        self._write("I")
        self._write(f"{x2}/{y2}K", 0)
        self._write("H")

    def rect(self, x, y, width, height):
        self._write(f"{x}/{y}K")
        self._write("I")
        self._write(f"{x+width}/{y}K", 0)
        self._write(f"{x+width}/{y+height}K", 0)
        self._write(f"{x}/{y+height}K", 0)
        self._write(f"{x}/{y}K", 0)
        self._write("H")

    def circle(self, x, y, radius):
        self._write(f"{x}/{y}K")
        self._write("I")
        self._write(f"O {radius}", 0)
        self._write("H")
    
    def arc(self, x, y, radius, startAngle, finalAngle):
        self._write(f"{x}/{y}K")
        self._write("I")
        self._write(f"O {radius} {startAngle} {finalAngle}", 0)
        self._write("H")
    
    def text(self, x, y, text, width=40, height=80, angle=0, slant=0):
        self._write(f"{x}/{y}K")
        self._write(f"Z {height} {angle} {width}")
        self._write(f"% {slant}") # 0 = normal, 1 = 75° slant
        self._write(f"B{text}")
        self._write('\r')

    def hershey(self, x, y, text, font="Serif", size=64, deg=0, xscale=1, yscale=1):
        print(x, y, text, font, size, deg)
        if font not in hershey:
            return

        self._write(f"{x}/{y}K")

        size = size/6400
        xpos = x
        ypos = y
        xoffset = math.inf
        yoffset = math.inf

        # get coordinates
        cmd1 = subprocess.run(['printf', '.PS\n "'+text+'" \n.PE'], capture_output=True)
        cmd2 = subprocess.run(['pic2plot', '-F', 'Hershey'+font, '-f', str(size), '-T', 'hpgl', '--rotation', str(deg)], input=cmd1.stdout, capture_output=True)
        hpgl = cmd2.stdout.decode('utf8')
        commands = hpgl.split(';')

        # find smallest x coordinate (huh)
        for command in commands:
            instruction = command[:2]
            if instruction == 'PA':
                if len(command) > 2:
                    coordinates = command[2:].split(',')
                    for i in range(0, len(coordinates), 2):
                        x,y = int(coordinates[i]),int(coordinates[i+1])
                        if x and x < xoffset:
                            xoffset = x
                        if y and y < yoffset:
                            yoffset = y

        # plot coordinates
        for command in commands:
            instruction = command[:2]
            if instruction == 'PU':
                self._write("H")
                #pen.penup # turtle test
            elif instruction == 'PD':
                self._write("I")
                #pen.pendown # turtle test
            elif instruction == 'PA':
                if len(command) > 2:
                    coordinates = command[2:].split(',')
                    for i in range(0, len(coordinates), 2):
                        x,y = int(coordinates[i]),int(coordinates[i+1])
                        nx = round((x-xoffset)*xscale+xpos)
                        ny = round((y-yoffset)*yscale+ypos)
                        print(x,y,':',nx,ny)
                        #if nx in range(0,self.width) and ny in range(0,self.height):
                        self._write(f"{nx}/{ny}K", 0.05)
                            #pen.goto(nx,ny) # turtle test



