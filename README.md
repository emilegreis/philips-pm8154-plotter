# Philips PM8154 Plotter

Documentation, scripts, experiments, etc. related to the Philips PM8154 Plotter (1983?). This plotter doesn't support [HP-GL](https://en.wikipedia.org/wiki/HP-GL).

![PM8154](snapshots/DSC_1788_cropped_2000.jpg "PM8154")

## Set up

With a USB to DB25 cable the plotter should connect to /dev/ttyUSB0

    ls /dev/tty*

Add user to uucp (Arch) or dialout (Debian) group, you may need to relog or even restart your computer

    usermod -a -G uucp username

Choose `PRINT` (to print ASCII text) or `PLOT` (see the [manual](https://gitlab.com/emilegreis/philips-pm8154-plotter/-/blob/main/resources/PM8154.pdf) for the plot commands) mode then hit `RESET` and `HOLD PAPER` buttons before sending commands

For example take pen 1

    echo -e "F1" > /dev/ttyUSB0

Or move to 1000/1000 coordinates

    echo -e "1000/1000K" > /dev/ttyUSB0

## Use

The `pm8154.py` file contains a basic serial interface to use the plotter (based on this PM8155 [script](https://github.com/jplattel/philips-pm8155-plotter))

Import it in your python script and start plotting 

    from pm8154 import PM8154

    plotter = PM8154()

    plotter.rect(200,100,100,200)
    plotter.line(300,300,400,400)
    plotter.hershey(500,500,'PM8154', 'Serif-Bold', 128)

The hershey function use pic2plot to get the Hershey font coordinates so you need to install gnuplot if you want to use it, the available Hershey fonts are:

    Serif
    Serif-Italic
    Serif-Bold
    Serif-BoldItalic
    Cyrillic
    Cyrillic-Oblique
    EUC
    Sans
    Sans-Oblique
    Sans-Bold
    Sans-BoldOblique
    Script
    Script-Bold
    GothicEnglish
    GothicGerman
    GothicItalian
    SerifSymbol
    SerifSymbol-Oblique
    SerifSymbol-Bold
    SerifSymbol-BoldOblique
    SansSymbol
    SansSymbol-Oblique

`hpgl-2-pm8154.py` is a very basic HP-GL converter, make a new 230mm x 180mm document with inkscape > save as .hpgl > set Resolution X and Y to 254 dpi, overcut to 0, offset correction to 0 then pipe your file in the script

    cat inputfile.hpgl | ./hpgl-2-pm8154.py
